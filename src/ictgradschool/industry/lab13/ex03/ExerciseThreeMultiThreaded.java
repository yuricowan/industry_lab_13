package ictgradschool.industry.lab13.ex03;

/**
 * TODO Create a multi-threaded program to calculate PI using the Monte Carlo method.
 */


public class ExerciseThreeMultiThreaded extends ExerciseThreeSingleThreaded  {

    /**
     * Estimates PI using a multi-threaded Monte Carlo method.
     */
    @Override
    protected double estimatePI(long numSamples) {

        int numberOfThreads = 700;
        double[] piResults = new double[numberOfThreads];
        Thread[] threads = new Thread[numberOfThreads];

        //must make samplesthread final, meaning it must stay the same to be called within the anonymous function
        final long samplesThread = numSamples / numberOfThreads;

        for (int i = 0; i < numberOfThreads; i++) {

            final int piThread = i;

            threads[i] = new Thread(new Runnable() {
                @Override
                public void run() {
                    piResults[piThread] = ExerciseThreeMultiThreaded.super.estimatePI(samplesThread);

                }
            });

            threads[i].start();
        }

        double finalResult = 0;

        for (int i = 0; i < numberOfThreads; i++) {
            try {
                threads[i].join();
                finalResult += piResults[i];


            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return finalResult / numberOfThreads;

    }

    /**
     * Program entry point.
     */


    public static void main(String[] args) {
        new ExerciseThreeMultiThreaded().start();
    }


}
