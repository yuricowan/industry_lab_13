package ictgradschool.industry.lab13.ex05;

import ictgradschool.industry.lab13.examples.example03.PrimeFactors;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ycow194 on 8/05/2017.
 */
public class PrimeFactorsTask implements Runnable {


    private Long n;
    private List<Long> primeFactors;
    private TaskState currentState;


    public PrimeFactorsTask(long n) {
        this.n = n;
        this.primeFactors = new ArrayList<>();
        this.currentState = TaskState.Initialized;
    }

    public long n() {
        return n;
    }

    public TaskState getState() {
        return currentState;
    }

    public List<Long> getPrimeFactors() {

        if (currentState.equals(TaskState.Completed)) {
            return primeFactors;
        }
        throw new IllegalStateException("Thread Aborted");

    }


    @Override
    public void run() {


        long n = n();

        System.out.print("The prime factorization of " + n + " is: ");


        // for each potential factor
        for (long factor = 2; factor * factor <= n; factor++) {
            if (!Thread.currentThread().isInterrupted()) {
                // if factor is a factor of n, repeatedly divide it out
                while (n % factor == 0) {
                    if (!Thread.currentThread().isInterrupted()) {
                        System.out.print(factor + " ");
                        primeFactors.add(factor);
                        n = n / factor;
                    }
                }
            }
            if (Thread.currentThread().isInterrupted()) {
                System.out.println("Program has been terminated");
                break;
            }
        }

        // if biggest factor occurs only once, n > 1
        if (n > 1) {
            System.out.println(n);
            primeFactors.add(n);
        } else {
            System.out.println();
        }
        if(!Thread.currentThread().isInterrupted()) {
            this.currentState = TaskState.Completed;
        }


    }

}
