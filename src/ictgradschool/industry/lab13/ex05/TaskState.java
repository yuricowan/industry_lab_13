package ictgradschool.industry.lab13.ex05;

/**
 * Created by ycow194 on 8/05/2017.
 */
public enum TaskState {
    Initialized, Completed, Aborted
}
