package ictgradschool.industry.lab13.ex05;

import ictgradschool.Keyboard;

/**
 * Created by ycow194 on 8/05/2017.
 */
public class PrimeFactorThread {
    public static void main(String[] args) {

        System.out.println("Please enter a number: ");
        Long n = Long.parseLong(Keyboard.readInput());


        PrimeFactorsTask primeFactor = new PrimeFactorsTask(n);
        Thread thread = new Thread(primeFactor);


        Thread cancelThread = new Thread(new Runnable() {
            @Override
            public void run() {
                // EXIT CODE
                String userRequest = "";
                userRequest = Keyboard.readInput();
                if (userRequest.equals("exit")) {
                    thread.interrupt();
                }
                // EXIT CODE
            }
        });


        thread.start();
        cancelThread.start();

        System.out.println("Type 'exit' if you wish to exit the program");

        // 1-Main thread which holds other 2
        // primeFactors thread
        // cancel


        try {
            thread.join();
            System.out.println("results: " + primeFactor.getPrimeFactors());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            System.out.println(e.getMessage());
        }
    }

}
