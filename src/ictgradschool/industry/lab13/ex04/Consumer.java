package ictgradschool.industry.lab13.ex04;

import java.util.concurrent.BlockingQueue;

/**
 * Created by ycow194 on 8/05/2017.
 */
public class Consumer implements Runnable {

    private BlockingQueue<Transaction> queue;
    private BankAccount account;

    public Consumer(BlockingQueue<Transaction> queue, BankAccount account) {
        this.queue = queue;
        this.account = account;
    }

    @Override
    public void run() {
        try {
            while (true) {

                Transaction transaction = queue.take();

                switch (transaction._type) {
                    case Deposit:
                        account.deposit(transaction._amountInCents);
                        break;
                    case Withdraw:
                        account.withdraw(transaction._amountInCents);
                        break;
                }

            }

        } catch (InterruptedException e) {

        }

    }
}
