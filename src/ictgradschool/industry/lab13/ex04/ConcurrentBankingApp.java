package ictgradschool.industry.lab13.ex04;

import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.BlockingQueue;

/**
 * Created by ycow194 on 1/05/2017.
 */
public class ConcurrentBankingApp {


    public static void main(String[] args) {
        final BlockingQueue<Transaction> queue = new ArrayBlockingQueue<>(10);
        BankAccount account = new BankAccount();
        int consumerThreads = 2;
        Thread[] threads = new Thread[consumerThreads];

        for (int i = 0; i < consumerThreads; i++) {


            threads[i] = new Thread(new Consumer(queue, account));
            threads[i].start();


        }


        Thread producer = new Thread(new Runnable() {
            @Override
            public void run() {
                // List transaction objects using transaction generator
                List<Transaction> transactions = TransactionGenerator.readDataFile();
                for (int i = 0; i < transactions.size(); i++) {

                    try {
                        queue.put(transactions.get(i));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
                // add to blocking queue object put(); and take(); methods

            }
        });
        producer.start();
        try {
            producer.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < consumerThreads; i++) {
            threads[i].interrupt();
            try {
                threads[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("final balance: " + account.getFormattedBalance());

    }
}
